#  Web Recovery UI

The Web UI prompts the user for a backup file, the URL of a backend server and a password file. Once a valid backup file along with the correct password and the correct URL has been provided, the web UI is going to show the user the last known location of their device on OpenStreetMaps.

<img src="./UI.png" height="500px">

Aside from allowing the user to see their device's last known location and the time of that location detection, the web UI also gives the user to option to send a wipe command, which tells the device to perform a factory reset, as soon as it gets fetched by the device.

## Technical details

The web UI consists of JavaScript file and an index.html file. No build steps are required to use this UI. It's sufficient to put those files in a directory that gets served by a webserver like nginx.

For convenience there is however a vite setup, that comes bundled with the repository. Because of that you can also start a web server by executing the following commands:

<code>npm install
npm run start
</code>

For cryptographic operation, the web UI uses the browser's [SubtleCrypto API](https://developer.mozilla.org/en-US/docs/Web/API/SubtleCrypto). Everything is done on the client side.

The cryptographic functions that are implemented in the web UI are comparable to the same ones that are in the Android app. See the Android app's readme file for more information.


