document.getElementById('locate_button').addEventListener('click', readBackupFileAndLocateDevice, false);
document.getElementById('wipe_button').addEventListener('click', wipeDevice, false);

function stringToArrayBuffer(str) {
  let bytes = new Uint8Array(str.length);
  for (var i = 0; i < str.length; i++) {
      bytes[i] = str.charCodeAt(i);
  }
  return bytes.buffer;
}

function base64ToArrayBuffer(base64) {
  let binaryString = atob(base64);
  return stringToArrayBuffer(binaryString);
}

function arrayBufferToBase64(arrayBuffer) {
  return btoa(String.fromCharCode(...new Uint8Array(arrayBuffer)));
}

async function decryptAndVerify(encryptedData, keyEncryption, keyMac) {
  let dec = new TextDecoder();
  let decryptedData = JSON.parse(dec.decode(await window.crypto.subtle.decrypt({
    "name": "AES-CBC",
    "iv": base64ToArrayBuffer(encryptedData.iv)
  }, keyEncryption, base64ToArrayBuffer(encryptedData.data))));

  if(await window.crypto.subtle.verify("HMAC", keyMac, base64ToArrayBuffer(decryptedData.mac), base64ToArrayBuffer(decryptedData.data))) {
    return atob(decryptedData.data);
  }
}

async function macAndEncrypt(data, keyEncryption, keyMac) {
  let enc = new TextEncoder();
  let mac = await window.crypto.subtle.sign("HMAC", keyMac, stringToArrayBuffer(data))
  let authenticatedData = {
    data: btoa(data),
    mac: arrayBufferToBase64(mac),
    algorithm: "HmacSHA256"
  };

  let iv = new Uint8Array(16);
  window.crypto.getRandomValues(iv);

  let encryptedData = await window.crypto.subtle.encrypt({
    "name": "AES-CBC",
    "iv": iv
  }, keyEncryption, enc.encode(JSON.stringify(authenticatedData)));

  let encryptedDataWithParams = {
    data: arrayBufferToBase64(encryptedData),
    transformation: "AES/CBC/PKCS5PADDING", // the padding seems to be correct according to https://stackoverflow.com/a/54751543
    iv: arrayBufferToBase64(iv)
  };

  return encryptedDataWithParams;
}

async function getBackupFile() {
  let files = document.getElementById("file").files;
  let file = files[0];
  let backupFile = JSON.parse(await file.text());
  return backupFile;
}

// returns a Promise that resolves to a tuple of keys: [macKey, encryptionKey]
async function getMacAndEncryptionKey(backupFile) {
  let password = document.getElementById("password").value;

  // A placeholder value is required in case of an empty password.
  // Otherwise the key derivation is going to fail on Android
  password = password == "" ? "empty" : password;

  let enc = new TextEncoder();
  let keyDecryptionKey = await window.crypto.subtle.importKey(
    "raw",
    enc.encode(password),
    { name: "PBKDF2" },
    false,
    ["deriveBits", "deriveKey"],
  );

  // reference: https://developer.mozilla.org/en-US/docs/Web/API/Pbkdf2Params
  // TODO parse hardcoded parameters from the config file as well
  let params = {
    "name": "PBKDF2",
    "hash": "SHA-256",
    "salt": base64ToArrayBuffer(backupFile.salt),
    "iterations": backupFile.iterationCount
  };

  let keyBits = await window.crypto.subtle.deriveBits(params, keyDecryptionKey, 256);
  let keyEncryption = await window.crypto.subtle.importKey(
    "raw",
    keyBits,
    "AES-CBC",
    false,
    ["decrypt", "encrypt"],
  );

  let keyMac = await window.crypto.subtle.importKey(
    "raw",
    keyBits,
    {
      "name": "HMAC",
      "hash": "SHA-256"
    },
    false,
    ["verify", "sign"],
  );

  return [keyMac, keyEncryption];
}

async function wipeDevice() {
  if(confirm("Are you sure that you want to wipe your device? All data on it will be irrecoverably lost!")) {
    let backupFile = await getBackupFile();
    let [keyMac, keyEncryption] = await getMacAndEncryptionKey(backupFile);
    let accountUuid = await decryptAndVerify(backupFile.encryptedData, keyEncryption, keyMac);
    let encryptedCommand = await macAndEncrypt("wipe", keyEncryption, keyMac);

    let headers = {
      "Content-Type": "application/json"
    };

    await fetch(document.getElementById("url").value + "/" + accountUuid + "/1", {method: "POST", body: JSON.stringify(encryptedCommand), headers: headers});
    alert("Wipe command has been sent!");
  }
}

async function readBackupFileAndLocateDevice(event) {

  let backupFile = await getBackupFile();
  let [keyMac, keyEncryption] = await getMacAndEncryptionKey(backupFile);
  let accountUuid = await decryptAndVerify(backupFile.encryptedData, keyEncryption, keyMac);

  let history = await (await fetch(document.getElementById("url").value + "/" + accountUuid + "/0")).json();
  console.log(history);

  let latestLocation = null;

  for(let element of history) {
    // use try catch to ensure that valid location data gets processed even if the response contains invalid location data
    try {
      for(let location of JSON.parse(await decryptAndVerify(element, keyEncryption, keyMac))) {
        console.log(location);

        if(latestLocation == null || latestLocation.time < location.time) {
          latestLocation = location;
        }
        
      }
    } catch(e) {
      console.trace(e)
    }
  }

  document.getElementById("title").innerText = "Last known location displayed is from: " + new Date(latestLocation.time);
  document.getElementById("mapdiv").style = "display:block;height:700px";
  map = new OpenLayers.Map("mapdiv");
  map.addLayer(new OpenLayers.Layer.OSM());
  
  var lonLat = new OpenLayers.LonLat(latestLocation.longitude, latestLocation.latitude)
        .transform(
          new OpenLayers.Projection("EPSG:4326"), // transform from WGS 1984
          map.getProjectionObject() // to Spherical Mercator Projection
        );
        
  var zoom=16;
  
  var markers = new OpenLayers.Layer.Markers("Markers");
  map.addLayer(markers);
  
  markers.addMarker(new OpenLayers.Marker(lonLat));
  
  map.setCenter (lonLat, zoom);

}

